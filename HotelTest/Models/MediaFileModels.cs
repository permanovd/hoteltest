﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using HotelTest.Infrastructure;

namespace HotelTest.Models
{
    [Table("MediaFiles")]
    public class MediaFile : EntityClass
    {        
        [DataType(DataType.ImageUrl)]
        public string Location { get; set; }

        public int MediaFileTypeID { get; set; }
        public  virtual MediaFileType MediaFileType { get; set; }
        
        public double MediaFileSize { get; set; }
        //[DataType(DataType.DateTime)]
        //public DateTime UploadDate { get; set; }
        //@todo implement date time field for any IEntity
    }

    [TableName("MediaFileType")]
    public class MediaFileType : EntityClass
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    [TableName("MediaFilesToEntities")]
    public class MediaFileToEntity
    {
        public int ID { get; set; }

        public int MediaFileID { get; set; }
        public virtual MediaFile MediaFile { get; set; }

        public int EntityTypeID { get; set; }
        public virtual EntityType EntityType { get; set; }

        public int BindingEntityId { get; set; }
        //public EntityClass BindingEntity { get; set; }
    }

}