﻿using System.ComponentModel.DataAnnotations.Schema;
using HotelTest.Infrastructure;

namespace HotelTest.Models
{
    [Table("Cities")]
    public class City : EntityClass
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CoorX { get; set; }
        public int CoorY { get; set; }
    }
}