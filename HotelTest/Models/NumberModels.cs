﻿using System.Collections.Generic;
using HotelTest.Infrastructure;

namespace HotelTest.Models
{
    public class Number : EntityClass
    {

        public int NumberTypeID { get; set; }
        public virtual IEnumerable<NumberType> NumberTypes { get; set; }

    }

    public class NumberType : EntityClass
    {
        
    }


}