﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Web.Mvc;
using HotelTest.Infrastructure;

namespace HotelTest.Models
{

    [Table("Hotels")]
    public class HotelFromDatabase : EntityClass
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Name is requered")]
        [StringLength(120)]
        public string Name { get; set; }

        [Required(ErrorMessage = "City is requered")]
        public int? CityID { get; set; }
        public virtual City City { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        //public virtual ICollection<HotelRatings> HotelRatings { get; set; }
        //public virtual ICollection<Number> Numbers { get; set; }
        //public virtual ICollection<Rating> Ratings { get; set; }
    }

    //public class HotelComplexEntity : IComplexEntity
    //{
    //    public IEntityType EntityType { get; set; }

    //    public int ID { get; set; }

    //    public string Name { get; set; }

    //    public virtual City City { get; set; }

    //    public string Description { get; set; }
    //}
}
