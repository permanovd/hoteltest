﻿using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.Mvc;
using HotelTest.Infrastructure;

namespace HotelTest.Models
{
    [TableName("FieldEntityTypes")]
    public class FieldEntityType :  IEntityType
    {
        [AllowHtml]
        public string Description { get; set; }

        public bool IsVisible { get; set; }

        public bool PlacedInHead { get; set; }

        [MaxLength(1000)]
        [AllowHtml]
        public string Markup { get; set; }

        public int ID { get; set; }
        public string Name { get; set; }
    }

    [TableName("FieldEntities")]
    public class FieldEntity : IEntity
    {
        public int FieldEntityTypeId { get; set; }
        public virtual FieldEntityType FieldEntityType { get; set; }

        public int EntityTypeId { get; set; }
        public virtual EntityType EntityType { get; set; }

        public int BindingEntityId { get; set; }
        //public virtual EntityClass BindingEntity { get; set; }

        public string FieldValue { get; set; }

        public string FieldClass { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
    }

    //[TableName("FieldsToEntities")]
    //public class FieldsToEntities
    //{
    //    public int ID { get; set; }

    //    public int EntityTypeID { get; set; }
    //    public virtual EntityType EntityType { get; set; }

    //    public int BindingEntityId { get; set; }
    //}
}