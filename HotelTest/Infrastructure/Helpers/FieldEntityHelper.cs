﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelTest.DAL;
using HotelTest.Models;
using HotelTest.ViewModels.FieldEntity;

namespace HotelTest.Infrastructure.Helpers
{
    public class FieldEntityHelper
    {

        public static IEnumerable<RenderedFieldEntityViewModel> GetFieldsForEntity(UnitOfWork unitOfWork,
            int entityId,int entityTypeId, int? fieldEntityTypeId = null)
        {
            var fieldCollection = unitOfWork.FieldEntities.GetMany(x => x.EntityTypeId == entityTypeId && x.BindingEntityId == entityId).ToList();
            var result = MapToViewModelAndRenderMarkup(fieldCollection,unitOfWork);

            return result;
        }

        private static IEnumerable<RenderedFieldEntityViewModel> MapToViewModelAndRenderMarkup(IEnumerable<FieldEntity> fieldCollection,UnitOfWork unitOfWork)
        {
            if (fieldCollection != null)
            {
                List<RenderedFieldEntityViewModel> retEntityViewModels = new List<RenderedFieldEntityViewModel>();
                string renderedFieldString = "";

                //unable to do multiple requests to db in loop
                //so we gonna try to do it that way
                List<FieldEntityType> typeList = FormFieldEntityTypeList(fieldCollection,unitOfWork);

                foreach (var fieldEntity in fieldCollection)
                {
                    var entity = fieldEntity;
                    if (typeList != null)
                        renderedFieldString = RenderField(typeList.FirstOrDefault(x => x.ID == fieldEntity.FieldEntityTypeId).Markup, fieldEntity.FieldValue, fieldEntity.FieldClass);
                    retEntityViewModels.Add(new RenderedFieldEntityViewModel{ID = fieldEntity.ID,Name = fieldEntity.Name,RenderedField = renderedFieldString});
                }
                return retEntityViewModels;
            }
            return null;
        }

        private static List<FieldEntityType> FormFieldEntityTypeList(IEnumerable<FieldEntity> fieldCollection, UnitOfWork unitOfWork)
        {
            var collList = fieldCollection;
            var allTypes = unitOfWork.FieldEntityTypes.GetAll().ToList();
            var retList = (from fieldEntity in collList from fieldEntityType in allTypes where fieldEntityType.ID == fieldEntity.FieldEntityTypeId select fieldEntityType).Distinct().ToList();
            return retList;
        }

        private static string _classStringInFieldMarkup = "$$class$$";
        private static string _valueStringInFieldMarkup = "$$value$$";
        //public string ClassString { get; set; }

        public static string RenderField(string markup, string fieldValue, string fieldClass)
        {
            var internalMarkup = markup;
            internalMarkup = internalMarkup.Replace(ClassStringInFieldMarkup, fieldClass);
            internalMarkup = internalMarkup.Replace(ValueStringInFieldMarkup, fieldValue);

            return internalMarkup;
        }

        public static string ClassStringInFieldMarkup
        {
            get { return _classStringInFieldMarkup; }
            set { _classStringInFieldMarkup = value; }
        }

        public static string ValueStringInFieldMarkup
        {
            get { return _valueStringInFieldMarkup; }
            set { _valueStringInFieldMarkup = value; }
        }
    }
}