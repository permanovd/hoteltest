﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using HotelTest.DAL;
using HotelTest.Models;

namespace HotelTest.Infrastructure.Helpers.MediaFile
{
    /// <summary>
    /// Helper class for file managment, static methods mostly
    /// </summary>
    public class MediaFileHelper
    {
        //@todo imlement

        public static IEnumerable<Models.MediaFile> UploadFilesAndCreateDatabaseEntries(UnitOfWork unit, HttpFileCollectionBase files, int entityId, int entityTypeId)
        {
            if (files != null)
            {
                if (files.Count > 0)
                {
                    List<Models.MediaFile> fileModels = new List<Models.MediaFile>();
                    
                    var entityType = unit.EntityTypes.GetByID(entityTypeId);
                    for (int i = 0; i < files.Count; i++)
                    {
                        var file = files[i];
                        if (file != null && file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                        {
                            var filename = file.FileName;
                            var mediaFileType = unit.MediaFileTypes.Get(x =>x.Name == file.ContentType).FirstOrDefault();
                            if (mediaFileType == null)
                            {
                                mediaFileType = unit.MediaFileTypes.Add(new MediaFileType { Name = file.ContentType });
                                unit.Commit();  //костыыыыыыыыыыыыыыыыыыыыыыыыль
                            }
                            var fileLocationString = "/MediaFiles/" + entityType.Name + "/" + entityId + "/" + mediaFileType.Name + "/"+filename;
                            var dirlocationString = HttpContext.Current.Server.MapPath("~/MediaFiles/" + entityType.Name + "/" + entityId + "/" + mediaFileType.Name + "/");
                            var fileSize = file.ContentLength;
                            var fileModel = new Models.MediaFile { Name = filename, MediaFileTypeID = mediaFileType.ID, Location = fileLocationString, MediaFileSize = fileSize };
                            if (!Directory.Exists(dirlocationString))
                            {
                                Directory.CreateDirectory(dirlocationString);
                            }

                            fileModels.Add(fileModel);

                            file.SaveAs(dirlocationString + filename);
                            unit.MediaFiles.Add(fileModel);
                        }
                    }
                    unit.Commit();
                    return fileModels;
                }
            }
            return null;
        }

        private static void BindUploadedFileToEntity(UnitOfWork unit, IEnumerable<Models.MediaFile> mediaFiles, int entityId, int entityTypeId)
        {
            if (mediaFiles != null)
            {
                foreach (var mediaFile in mediaFiles)
                {
                    unit.MediaFilesToEntites.Add(new MediaFileToEntity { EntityTypeID = entityTypeId, MediaFileID = mediaFile.ID,BindingEntityId = entityId});
                }
            }
        }

        public static void UploadAndBindMediaFiles(UnitOfWork unit, HttpFileCollectionBase files, int entityId, int entityTypeId)
        {
            var fileEntities = UploadFilesAndCreateDatabaseEntries(unit, files,entityId, entityTypeId);
            if (fileEntities != null)
            {
                BindUploadedFileToEntity(unit, fileEntities,entityId,entityTypeId);
                unit.Commit();
            }
        }
        /// <summary>
        /// Returns list of entries of MediaFile type
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityTypeId"></param>
        /// <param name="fileTypeId">File type id, MediaFileType.ID</param>
        /// <param name="unit"></param>
        /// <returns>MediaFile entities collection</returns>
        public static IEnumerable<Models.MediaFile> GetMediaFilesForEntity(int entityId,int entityTypeId,int fileTypeId, UnitOfWork unit)
        {
            List<Models.MediaFile> mediaFiles= new List<Models.MediaFile>();
            var entries = unit.MediaFilesToEntites.GetMany(x => x.EntityTypeID == entityTypeId && x.BindingEntityId == entityId).ToList();
            foreach (var entry in entries)
            {
                mediaFiles.Add(unit.MediaFiles.GetByID(entry.MediaFileID));
            }
            mediaFiles = mediaFiles.Where(f => f.MediaFileTypeID == fileTypeId).ToList();
            return mediaFiles;
        } 
    }

}