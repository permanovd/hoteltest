﻿namespace HotelTest.Infrastructure.Factories
{
    public interface IEntityTypeFactory
    {
        IEntityType GetEntityType();

        int GetEntityTypeId();

        string GetEntityTypeName();

        string GetEntityDescription();

    }
}