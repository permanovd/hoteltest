﻿using System.Collections.Generic;

namespace HotelTest.Infrastructure.Factories
{
    public interface IComplexEntityFactory<TBase,TDerived> 
        where TBase : IEntity 
        where TDerived : TBase
    {
        TDerived GetComplexEntityFromDatabaseEntity(TBase baseEntity);
        IEnumerable<TDerived> GetComplexEntitiesFromDatabaseEntitiesBulk(IEnumerable<TBase> baseEntities);
    }
}