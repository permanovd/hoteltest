﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HotelTest.DAL;

namespace HotelTest.Infrastructure.Repositories
{
    public class LightWeightRepository<TEntity>
        where TEntity : class, new()
    {
        private readonly HotelContext context;
        private readonly DbSet<TEntity> dbSet;

        public LightWeightRepository(HotelContext cont)
        {
            context = cont;
            dbSet = context.Set<TEntity>();
        }

        public TEntity Add(TEntity entity)
        {
            return dbSet.Add(entity);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public void Delete(int Id)
        {
            var entityToDelete = dbSet.Find(Id);
            Delete(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public TEntity GetById(long Id)
        {
            var entity = dbSet.Find(Id);
            return entity;
        }

        public IEnumerable<TEntity> GetMany(Func<TEntity, bool> filter)
        {
            IEnumerable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return dbSet;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}