﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HotelTest.DAL;
using HotelTest.Models;

namespace HotelTest.Infrastructure.Repositories
{
    public class MediaFileTypeTypeRepository
    {
        private readonly HotelContext context;
        private readonly DbSet<MediaFileType> dbSet;

        public MediaFileTypeTypeRepository(HotelContext cont)
        {
            context = cont;
            dbSet = context.Set<MediaFileType>();
        }

        public void Add(MediaFileType entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(MediaFileType entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public void Delete(int Id)
        {
            var entityToDelete = dbSet.Find(Id);
            Delete(entityToDelete);
        }

        public virtual void Update(MediaFileType entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public MediaFileType GetById(long Id)
        {
            var entity = dbSet.Find(Id);
            return entity;
        }

        public IEnumerable<MediaFileType> GetMany(Func<MediaFileType, bool> filter)
        {
            IEnumerable<MediaFileType> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public IEnumerable<MediaFileType> GetAll()
        {
            return dbSet;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}