﻿namespace HotelTest.Infrastructure.Builders
{
    /// <summary>
    /// Builds objects for view models, where entities contain images or any other generic objects
    /// </summary>
    public interface IComplexEntityModelBuilder : IEntityModelBuilder
    {
        void BuildImagesForEntity(); // @todo implement Images entity
        //void BuildEntityTypeForString();
    }
}