﻿using System;

namespace HotelTest.Infrastructure.Builders
{
    public interface IEntityModelBuilder
    {
        Type ClassType { get; }

        void BuildEntityType(IEntityType entityType);
    }
}