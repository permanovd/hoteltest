﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HotelTest.DAL;
using HotelTest.Models;
using Microsoft.Ajax.Utilities;

namespace HotelTest.Infrastructure.Builders
{
    public class EntityModelBuilder<TEntity> : IEntityModelBuilder where TEntity : IComplexEntity, new()
    {
        private UnitOfWork _unitOfWork;
        private readonly IComplexEntity _entity;

        public EntityModelBuilder(UnitOfWork unit)
        {
            _unitOfWork = unit;
            _entity = new TEntity();
        }

        public Type ClassType
        {
            get
            {
                return _entity.GetType();
            }
        }

        public void BuildEntityType(IEntityType entity)
        {
            _entity.EntityType = entity;
        }

        public IComplexEntity GetResult()
        {
            return _entity;
        }
    }

    public class EntityModelDirector
    {
        private IEntityModelBuilder _builder;

        public EntityModelDirector(IEntityModelBuilder builder)
        {
            _builder = builder;
        }
        private IEntityType GetEntityType()
        {
            var classType = _builder.ClassType;
            string entityName = "";
            EntityTypeStructure retEntType = new EntityTypeStructure();

            using (var unit = new UnitOfWork())
            {
                entityName = unit.EntityTypes.GetEntityTypeName();
                var entityType = unit.EntityTypes.Get(x => x.Name == entityName).FirstOrDefault();
                if (entityType != null)
                {
                    retEntType.ID = entityType.ID;
                    retEntType.Name = entityType.Name;
                    return retEntType;
                }
            }
            return null;
        }

        public void BuildEntityModel()
        {
            _builder.BuildEntityType(GetEntityType());
        }
    }
}