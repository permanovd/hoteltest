﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace HotelTest.Infrastructure
{
    public interface IEntity
    {
        int ID { get; set; }

        string Name { get; set; }
    }

}