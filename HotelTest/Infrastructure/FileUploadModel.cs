﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelTest.Infrastructure
{
    public class FileUploadModel
    {
        public int EntityId { get; set; }
        public int EntityTypeId { get; set; }
    }
}