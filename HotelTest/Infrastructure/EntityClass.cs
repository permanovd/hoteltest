﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelTest.Models;
using HotelTest.ViewModels.FieldEntity;

namespace HotelTest.Infrastructure
{
    public abstract class EntityClass : IEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }

        private EntityTypeStructure type;

        public EntityTypeStructure GetEntityType()
        {
            return type;
        }

        public void SetEntityType(EntityTypeStructure val)
        {
            type = val;
        }

        private IEnumerable<RenderedFieldEntityViewModel> fieldEntities;

        public IEnumerable<RenderedFieldEntityViewModel> GetFieldEntities()
        {
            return fieldEntities;
        }

        public void SetFieldEntities(IEnumerable<RenderedFieldEntityViewModel> fieldCollection)
        {
            fieldEntities = fieldCollection;
        }

        private IEnumerable<MediaFile> fileEntities;

        public IEnumerable<MediaFile> GetFileEntities()
        {
            return fileEntities;
        }

        public void SetFileEntities(IEnumerable<MediaFile> fileCollection)
        {
            fileEntities = fileCollection;
        }

    }
}