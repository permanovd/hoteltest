﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelTest.Infrastructure
{
    public interface IEntityType : IEntity
    {
        string Description { get; set; }
    }
}