﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelTest.Infrastructure
{
    public interface IComplexEntity : IEntity
    {
        IEntityType EntityType { get; set; }
    }
}