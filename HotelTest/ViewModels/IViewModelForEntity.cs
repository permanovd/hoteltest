﻿namespace HotelTest.ViewModels
{
    public interface IViewModelForEntity
    {
        int ID { get; set; }
        string Name { get; set; }
    }
}