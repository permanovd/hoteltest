﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelTest.Models;

namespace HotelTest.ViewModels.FieldEntity
{
    public class RenderedFieldEntityViewModel : IEntityWithFilesViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<MediaFile> MediaFiles { get; set; }

        public string RenderedField { get; set; }


    }
}