﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelTest.ViewModels
{
    public interface IEntityWithFieldsViewModel
    {
        IEnumerable<Models.FieldEntity> FieldEntities { get; set; } 
    }
}