﻿using System.Collections.Generic;
using HotelTest.Models;

namespace HotelTest.ViewModels
{
    public interface IEntityWithFilesViewModel : IViewModelForEntity
    {
        IEnumerable<MediaFile> MediaFiles { get; set; } 
    }
}