﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using HotelTest.Infrastructure;
using HotelTest.Models;

namespace HotelTest.DAL
{

    public class HotelContext : DbContext
    {
        public HotelContext()
            : base("Hotels")
        {
            Database.SetInitializer<HotelContext>(new CreateDatabaseIfNotExists<HotelContext>());
        }

        public DbSet<MediaFile> MediaFiles { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<HotelFromDatabase> Hotels { get; set; }
        public DbSet<MediaFileType> MediaFileTypes { get; set; }
        public DbSet<FieldEntityType> FieldEntityTypes { get; set; }
        public DbSet<EntityType> EntityTypes { get; set; }
        public DbSet<FieldEntity> FieldEntities { get; set; }
        public DbSet<MediaFileToEntity> MediaFilesToEntites { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        //public DbSet<FieldsToEntities> FieldsToEntities { get; set; }
        //public DbSet<Field> Fields { get; set; }
        //public DbSet<FieldType> FieldTypes { get; set; }
        //public DbSet<Tags> Tags { get; set; }
        //public DbSet<EntitiesToTags> EntitiesToTags { get; set; }

    }
}