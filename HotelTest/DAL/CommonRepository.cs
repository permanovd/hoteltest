﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using HotelTest.Infrastructure;
using HotelTest.Infrastructure.Helpers;
using HotelTest.Infrastructure.Helpers.MediaFile;
using HotelTest.Models;

namespace HotelTest.DAL
{
    public class CommonRepository<TEntity>  where TEntity : EntityClass, new()
    {
        private HotelContext context;
        private DbSet<TEntity> dbSet;

        public CommonRepository(HotelContext cont)
        {
            context = cont;
            dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }

            if (query != null)
            {
                var entityType = GetEntityType();
                foreach (var entity in query)
                {
                    entity.SetEntityType(entityType);
                }
            }
            return query.ToList();
        }

        public virtual TEntity GetByID(int id)
        {
            var entityType = GetEntityType();
            var entity = dbSet.Find(id);
            entity.SetEntityType(entityType);
            //entity.SetFieldEntities(GetFieldEntities(id));
            return entity;
        }

        public virtual TEntity Add(TEntity entity)
        {
            return dbSet.Add(entity);
        }

        /// <summary>
        /// Creates entity if one not found by name,do not use for complex entities
        /// </summary>
        /// <param name="name"></param>
        /// <returns>IEntity</returns>
        public virtual TEntity FindOrCreateByName(string name,out bool found)
        {
            var entity = dbSet.FirstOrDefault(x => x.Name == name);
            if (entity != null)
            {
                found = true;
                return entity;
            }
            else
            {
                entity = dbSet.Add(new TEntity(){Name = name});
                found = false;
            }
            return entity;
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual EntityTypeStructure GetEntityType()
        {
            var typeName = GetEntityTypeName();
            var firstOrDefault = context.EntityTypes.FirstOrDefault(x => x.Name == typeName);
            if (firstOrDefault != null)
            {
                var typeId = firstOrDefault.ID;
                return new EntityTypeStructure{Name = typeName,ID = typeId};
            }
            return null;
        }

        public virtual string GetEntityTypeName()
        {
            string name = (context as IObjectContextAdapter).ObjectContext.CreateObjectSet<TEntity>().EntitySet.Name;
            return name.ToLower();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
