﻿using System;
using System.Diagnostics;
using HotelTest.Infrastructure.Repositories;
using HotelTest.Models;

namespace HotelTest.DAL
{
    public class UnitOfWork : IUnitOfWork,IDisposable
    {
        private readonly HotelContext _db = new HotelContext();
        private CommonRepository<HotelFromDatabase> hotels;
        private CommonRepository<City> cities;
        private CommonRepository<EntityType> entityTypes;
        private LightWeightRepository<FieldEntity> fieldEntities;
        private LightWeightRepository<FieldEntityType> fieldEntityTypes;
        private CommonRepository<MediaFile> mediaFiles;
        private CommonRepository<MediaFileType> mediaFileTypes;
        private LightWeightRepository<MediaFileToEntity> mediaFilesToEntites; 

        public CommonRepository<HotelFromDatabase> Hotels
        {
            get { return hotels ?? (hotels = new CommonRepository<HotelFromDatabase>(_db)); }
        }

        public CommonRepository<City> Cities
        {
            get { return cities ?? (cities = new CommonRepository<City>(_db)); }
        }

        public CommonRepository<EntityType> EntityTypes
        {
            get { return entityTypes ?? (entityTypes = new CommonRepository<EntityType>(_db)); }
        }

        public CommonRepository<MediaFile> MediaFiles
        {
            get { return mediaFiles ?? (mediaFiles = new CommonRepository<MediaFile>(_db)); }
        }

        public CommonRepository<MediaFileType> MediaFileTypes
        {
            get { return mediaFileTypes ?? (mediaFileTypes = new CommonRepository<MediaFileType>(_db)); }
        }

        public LightWeightRepository<MediaFileToEntity> MediaFilesToEntites
        {
            get { return mediaFilesToEntites ?? (mediaFilesToEntites = new LightWeightRepository<MediaFileToEntity>(_db)); }
        }

        public LightWeightRepository<FieldEntity> FieldEntities
        {
            get { return fieldEntities ?? (fieldEntities = new LightWeightRepository<FieldEntity>(_db)); }
        }

        public LightWeightRepository<FieldEntityType> FieldEntityTypes
        {
            get { return fieldEntityTypes ?? (fieldEntityTypes = new LightWeightRepository<FieldEntityType>(_db)); }
        }

        public void Commit()
        {
            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                Debug.WriteLine("askdajklsd");
                throw;
            }
        }

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                disposed = true;
            }
        }
    }
}