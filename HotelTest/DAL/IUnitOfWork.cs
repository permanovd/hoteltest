﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelTest.DAL
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}