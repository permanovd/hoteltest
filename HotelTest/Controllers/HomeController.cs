﻿using System.Web;
using System.Web.Mvc;
using HotelTest.DAL;
using HotelTest.Infrastructure;
using HotelTest.Infrastructure.Helpers;
using HotelTest.Infrastructure.Helpers.MediaFile;

namespace HotelTest.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        private UnitOfWork unitOfWork;

        public HomeController()
        {
            unitOfWork = new UnitOfWork();
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {
            var hotels = unitOfWork.Hotels.Get();
            return View(hotels);
        }

        //
        // GET: /Home/Details/5

        public ActionResult Details(int id)
        {
            var hotels = unitOfWork.Hotels.GetByID(id);
            hotels.SetFileEntities(MediaFileHelper.GetMediaFilesForEntity(hotels.ID,hotels.GetEntityType().ID,10,unitOfWork));
            hotels.SetFieldEntities(FieldEntityHelper.GetFieldsForEntity(unitOfWork,hotels.ID,hotels.GetEntityType().ID));
            return View(hotels);
        }


        // GET: 
        [HttpGet]
        [ActionName("FileUpload")]
        public ActionResult FileUpload()
        {
            return View();
        }

        [HttpPost]
        [ActionName("FileUpload")]
        public ActionResult FileUploadPost(FileUploadModel fileUploadModel)
        {
            HttpFileCollectionBase files = Request.Files;
            MediaFileHelper.UploadAndBindMediaFiles(unitOfWork, files, fileUploadModel.EntityId, fileUploadModel.EntityTypeId);
            return RedirectToAction("Details",new {id = fileUploadModel.EntityId});
        }
        //
        // GET: /Home/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Home/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Home/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
