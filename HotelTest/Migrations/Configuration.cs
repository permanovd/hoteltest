using HotelTest.Models;

namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HotelTest.DAL.HotelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HotelTest.DAL.HotelContext context)
        {
            //  This method will be called after migrating to the latest version.


            context.Cities.AddOrUpdate(p => p.Name,
                new City{ Name = "Moscow",CoorX = 202020,CoorY = 303030},
                new City { Name = "London", CoorX = 204020, CoorY = 301030 },
                new City { Name = "Ashgabat", CoorX = 203010, CoorY = 309060 }
                );

            context.Hotels.AddOrUpdate(p => p.Name,
                new HotelFromDatabase{CityID = 1,Name = "Ritz Carlton",Description = "nice hotel"},
                new HotelFromDatabase{CityID = 1,Name = "Pentonville",Description = "nice hotel"},
                new HotelFromDatabase{CityID = 2,Name = "SOB",Description = "nice hotel"}
                );
            //context.EntityTypes.SqlQuery("TRUNCATE TABLE dbo.EntityTypes");
            context.EntityTypes.AddOrUpdate(p => p.Name,
                new EntityType { Name = "hotels" },
                new EntityType { Name = "fieldentities" },
                new EntityType{Name = "cities"}
                );
            context.FieldEntityTypes.AddOrUpdate(p => p.Name,
                new FieldEntityType{Name = "Paragraph", Description = "p tag field",IsVisible = true,Markup = "<div class='sv-paragraph $$class$$'><p>$$value$$</p></div>",PlacedInHead = false},
                new FieldEntityType{Name = "Script tag", Description = "script tag in head",IsVisible = false,Markup = "<script type='text/javascript' src='$$value$$'></script>",PlacedInHead = true});

            context.FieldEntities.AddOrUpdate(p => p.Name,
                new FieldEntity { EntityTypeId = 1, FieldClass = "sv-paragraph1",BindingEntityId = 1,Name = "Category",FieldValue = "Hello world",FieldEntityTypeId = 1},
                new FieldEntity { EntityTypeId = 1, FieldClass = "script",BindingEntityId = 1,Name = "Head Tag",FieldValue = "/Content/bootstrap.js",FieldEntityTypeId = 2});
            
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
