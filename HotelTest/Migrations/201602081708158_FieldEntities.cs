namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FieldEntities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FieldEntityTypeId = c.Int(nullable: false),
                        EntityTypeId = c.Int(nullable: false),
                        FieldValue = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityTypes", t => t.EntityTypeId, cascadeDelete: true)
                .ForeignKey("dbo.FieldEntityTypes", t => t.FieldEntityTypeId, cascadeDelete: true)
                .Index(t => t.FieldEntityTypeId)
                .Index(t => t.EntityTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FieldEntities", "FieldEntityTypeId", "dbo.FieldEntityTypes");
            DropForeignKey("dbo.FieldEntities", "EntityTypeId", "dbo.EntityTypes");
            DropIndex("dbo.FieldEntities", new[] { "EntityTypeId" });
            DropIndex("dbo.FieldEntities", new[] { "FieldEntityTypeId" });
            DropTable("dbo.FieldEntities");
        }
    }
}
