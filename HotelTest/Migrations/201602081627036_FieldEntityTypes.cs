namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldEntityTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FieldEntityTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        IsVisible = c.Boolean(nullable: false),
                        PlacedInHead = c.Boolean(nullable: false),
                        Markup = c.String(maxLength: 1000),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FieldEntityTypes");
        }
    }
}
