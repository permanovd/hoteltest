namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldEntities2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FieldEntities", "FieldClass", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FieldEntities", "FieldClass");
        }
    }
}
