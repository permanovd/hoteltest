namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ThirdPart : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Cities", newName: "City");
            RenameTable(name: "dbo.EntityTypes", newName: "EntityType");
            RenameTable(name: "dbo.Hotels", newName: "Hotel");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Hotel", newName: "Hotels");
            RenameTable(name: "dbo.EntityType", newName: "EntityTypes");
            RenameTable(name: "dbo.City", newName: "Cities");
        }
    }
}
