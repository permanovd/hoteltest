namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MediaFiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Location = c.String(),
                        MediaFileTypeID = c.Int(nullable: false),
                        MediaFileSize = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MediaFileTypes", t => t.MediaFileTypeID, cascadeDelete: true)
                .Index(t => t.MediaFileTypeID);
            
            CreateTable(
                "dbo.MediaFileTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MediaFileToEntities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MediaFileID = c.Int(nullable: false),
                        EntityTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EntityTypes", t => t.EntityTypeID, cascadeDelete: true)
                .ForeignKey("dbo.MediaFiles", t => t.MediaFileID, cascadeDelete: true)
                .Index(t => t.MediaFileID)
                .Index(t => t.EntityTypeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MediaFileToEntities", "MediaFileID", "dbo.MediaFiles");
            DropForeignKey("dbo.MediaFileToEntities", "EntityTypeID", "dbo.EntityTypes");
            DropForeignKey("dbo.MediaFiles", "MediaFileTypeID", "dbo.MediaFileTypes");
            DropIndex("dbo.MediaFileToEntities", new[] { "EntityTypeID" });
            DropIndex("dbo.MediaFileToEntities", new[] { "MediaFileID" });
            DropIndex("dbo.MediaFiles", new[] { "MediaFileTypeID" });
            DropTable("dbo.MediaFileToEntities");
            DropTable("dbo.MediaFileTypes");
            DropTable("dbo.MediaFiles");
        }
    }
}
