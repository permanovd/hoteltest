namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldEntities3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FieldEntities", "BindingEntityId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FieldEntities", "BindingEntityId");
        }
    }
}
