// <auto-generated />
namespace HotelTest.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class FileToEntityFix : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FileToEntityFix));
        
        string IMigrationMetadata.Id
        {
            get { return "201602122131418_FileToEntityFix"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
