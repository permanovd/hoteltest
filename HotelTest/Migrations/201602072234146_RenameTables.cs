namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameTables : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.City", newName: "Cities");
            RenameTable(name: "dbo.EntityType", newName: "EntityTypes");
            RenameTable(name: "dbo.Hotel", newName: "Hotels");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Hotels", newName: "Hotel");
            RenameTable(name: "dbo.EntityTypes", newName: "EntityType");
            RenameTable(name: "dbo.Cities", newName: "City");
        }
    }
}
