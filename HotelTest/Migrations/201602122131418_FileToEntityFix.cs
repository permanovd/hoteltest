namespace HotelTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileToEntityFix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MediaFileToEntities", "BindingEntityId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MediaFileToEntities", "BindingEntityId");
        }
    }
}
