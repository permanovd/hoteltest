﻿using System;
using HotelTest.Infrastructure.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HotelTest.Tests
{
    [TestClass]
    public class FieldHelperTest
    {
        [TestMethod]
        public void RenderFieldTest()
        {
            //arange
            string markup = "<div class='sv-paragraph $$$class$$$'><p>$$value$$</p></div>";
            string fieldVal = "hello";
            string fieldClass = "field-1";

            //act
            FieldEntityHelper.ClassStringInFieldMarkup = "$$$class$$$";
            var result = FieldEntityHelper.RenderField(markup, fieldVal, fieldClass);

            //assert
            Assert.AreEqual("<div class='sv-paragraph "+fieldClass+"'><p>"+fieldVal+"</p></div>", result);
        }
    }
}
