﻿using System;
using HotelTest.Controllers;
using HotelTest.DAL;
using HotelTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HotelTest.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetTypeNameTest()
        {
            //arrange 
            var unit = new UnitOfWork();

            //act
            string typeName = unit.EntityTypes.GetEntityTypeName();

            //assert
            Assert.AreEqual(typeName, "EntityType");
        }

        [TestMethod]
        public void ControllerTest()
        {
            var controller = new HomeController();

            var index = controller.Index();

            Assert.IsNotNull(index);
        }
    }
}
