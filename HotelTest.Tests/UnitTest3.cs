﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using HotelTest.DAL;
using HotelTest.Infrastructure.Repositories;
using HotelTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HotelTest.Tests
{
    [TestClass]
    public class CommonRepositoryTests
    {
        [TestMethod]
        public void LightRepoAddTest()
        {
            //arrange
            var context = new HotelContext();
            var repo = new LightWeightRepository<MediaFileType>(context);
            
            //act
            var ent = repo.Add(new MediaFileType{Name = "azaza",Description = "ololo"});
            repo.Save();
            
            //assert
            Assert.IsNotNull(ent);
            Assert.AreEqual(ent.Name, "azaza");
            Assert.AreEqual(ent.Description, "ololo");
        }
        
        [TestMethod]
        public void CommonRepoGetTest()
        {
            //arrange
            //var context = new HotelContext();
            //var repo = new CommonRepository<MediaFileType>(context);
            //UnitOfWork unit = new UnitOfWork();

            ////act
            ////var ent = repo.Add(new MediaFileType { Name = "azaza", Description = "ololo" });
            //var ent = unit.Cities.Add(new City{Name = "azaza",CoorX = 2132132,CoorY = 231231});
            //unit.Commit();

            //var ents = unit.Cities.Get();
            var city = new City {Name = "azazamod", CoorX = 2132132, CoorY = 231231};
            var context = new HotelContext();
            var ent = context.Cities.Add(city);
            var ents = context.Cities.ToList();
            context.Entry(city).State = EntityState.Added;
            context.SaveChanges();

            context.Dispose();
            //unit._db.Cities.AddOrUpdate(new City {Name = "azazamod", CoorX = 2132132, CoorY = 231231});
            //unit._db.SaveChanges();

            //assert
            Assert.IsNotNull(ent);
            Assert.IsNotNull(ents);
            Assert.AreEqual(ent.Name, "azaza");
            //Assert.AreEqual(ent.Description, "ololo");
        }
    }
}
