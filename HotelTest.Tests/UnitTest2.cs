﻿using System;
using System.Web;
using HotelTest.DAL;
using HotelTest.Infrastructure.Helpers.MediaFile;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HotelTest.Tests
{
    [TestClass]
    public class MediaFilesTests
    {
        [TestMethod]
        public void FileUploadTest()
        {
            //arrange
            var unit = new UnitOfWork();
            Mock<HttpFileCollectionWrapper> fileMock = new Mock<HttpFileCollectionWrapper>();
            fileMock.Setup(x => x[0].ContentLength).Returns(It.IsAny<int>());
            fileMock.Setup(x => x[0].FileName).Returns(It.IsAny<string>());

            //act
            //var fileEntities = MediaFileHelper.UploadFilesAndCreateDatabaseEntries(unit,fileMock.Object,1);


            //assert
            //Assert.IsNotNull(fileEntities);

        }
    }
}
